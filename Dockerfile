FROM openlabs/docker-wkhtmltopdf-aas
RUN mkdir -p /usr/share/fonts/truetype
COPY ./fonts /usr/share/fonts/truetype